﻿using System;

namespace Program1
{
    class Program
    {
        //Project 1.1 LetterCount 
        public static string LetterCount(string str)
        {
            str = str.ToLower();
            var arr = str.Split(" ");
            var count = 0;
            var word = "-1";
            for (var i = 0; i < arr.Length; i++)
            {
                for (var a = 0; a < arr[i].Length; a++)
                {
                    var newCount = 0;
                    for (var b = a + 1; b < arr[i].Length; b++)
                    {
                        if (arr[i][a] == arr[i][b])
                            newCount += 1;
                    }
                    if (newCount > count)
                    {
                        count = newCount;
                        word = arr[i];
                    }
                }
            }
            return word;
        }

        //Project 1.2 WordSplit
        public static string WordSplit()
        {
            string[] strArr = { "hellocat", "apple,bat,cat,goodbye,hello,yellow,why" }; //return hello,cat
            //string[] strArr = { "hellocat", "apple,bat,goodbye,hello,yellow,why" }; //return Not possible

            var wordToCompare = strArr[0];
            var stringDictionary = strArr[1];  
            var singleArry = stringDictionary.Split(',');
            var answer = "Not Possible";

            foreach (var word in singleArry)
            {
                if (wordToCompare.Contains(word))
                {
                    var splitWord = wordToCompare.Split(word);
                    if (splitWord.Length > 0)
                    {
                        foreach (var split in splitWord)
                        {
                            foreach(var word2 in singleArry)
                            {
                                if(word2 == split)
                                {
                                    var joinedWord = word + split;
                                    var reversedWord = split + word;
                                    if (joinedWord == wordToCompare )
                                    {
                                        answer = word + "," + split;
                                    }else if(reversedWord == wordToCompare)
                                    {
                                        answer = split + "," + word;
                                    }
                                }
                            }
                                
                            
                        }
                    }
                }             
            }

            return answer;

        }

        static void Main(string[] args)
        {
            //var a  = LetterCount("Today, is the day !"); //Return -1 bcz no repeating
            var a = LetterCount("Today, is the greatest day ever!"); // return greatest
            Console.WriteLine(a);

            var b = WordSplit();
            Console.WriteLine(b);
        }
    }
}
